from django.db import models
from django.contrib.auth.models import User
import datetime
from django.db.models.signals import post_save
from django.core.files import File
import os.path
import djangotoolbox.fields as models2
from django.template.defaultfilters import slugify


class Note(models.Model): 
    name = models.CharField(max_length=100)
    description = models.TextField()
    creator = models.ForeignKey('UserProfile', related_name='projects')
    reward = models.FloatField(default=0)
    criteria = models.CharField(max_length=100)
    more_criteria = models.TextField()
    slug = models.SlugField()

    def __unicode__(self):
        return self.name

class Image(models.Model): 
    note = models.ForeignKey(Note, related_name='images')
    img = models2.BlobField() 
    
    def __unicode__(self):
        return str(self.id)

class UserProfile(models.Model):
    name = models.CharField(max_length=50)
    email = models.EmailField()
    user = models.OneToOneField(User, unique=True)
    signup_date = models.DateTimeField(default=datetime.datetime.today())

    def __unicode__(self):
        return self.name

